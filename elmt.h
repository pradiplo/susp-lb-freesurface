/*
 * File:   elmt.h
 * Author: aleonard
 *
 * Created on November 5, 2013, 4:11 PM
 */

#ifndef ELMT_H
#define	ELMT_H

#include "myvector.h"

/*//////////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS  DEFINITIONS
//////////////////////////////////////////////////////////////////////////////////////////////////////*/




class particle{
public:
    unsigned int index;
    // constitutive particles indexes
	
	unsigned int particleIndex;

    unsigned int size;
    // radius of constitutive particles (supposed, for the moment, of being constant)
    double r;
    // mass 
    double m;
    // moment of inertia
    double I;
	
    // Position and derivatives
    // position of the center of mass of the element
    tVect  x0;

    // velocity of the center of the element
    tVect x1;

	//neighbor list parameters
	
	int    list[50]; 
	int    _list[50];
	double xix[50];
	double xiy[50];
	double xiz[50];
	double _xix[50];
	double _xiy[50];
	double _xiz[50];
		 
	tVect xiw;
	tVect xio;
	tVect x0_lst;


    // for the verlet algorithm
    // mass 
    double _m;
    // moment of inertia
    double _I;
    tVect _x0;
	tVect xt0;
	tVect disp;
	
	double disp_r;
	
	double trueStrain;
	double engStrain;
	double trueMod;
	double engMod;
	
	double mu;
	double ratio;
	
	double pressure;
	double normal1;
	double normal2;
	
    // Orientation and derivatives
    // orientation of the center of mass of the element
    tQuat q0, q1;
    // quaternion rates (related to angular velocity, acceleration...)
    // angular velocity rates (global)

    tVect w0,w1,wGlobal;

    // forces and moments
    tVect FHydro,FContact,FTotal,FLub,FGrav,FRep;
    tVect MHydro,MContact,MTotal,MLub;
	tMat SHydro, SLub, SContact, SRep, STotal;
   
    // fluid mass entrained by the particle
    double fluidVolume;
    	
	
	particle() {
		
		index=0;
		particleIndex =  0;
        size=1;
        r=1.0;
        m=1.0;
        I=1.0;
		
        disp=x0=x1=xiw=xio=x0_lst=xt0=_x0=tVect(0.0,0.0,0.0);
        
		pressure = normal1 = normal2 = disp_r = 0.0;
		
		
		q0=tQuat(1.0,0.0,0.0,0.0);
        q1=tQuat(0.0,0.0,0.0,0.0);

        w0=w1=tVect(0.0,0.0,0.0);
        
		list[0] = 0; 
		_list[0]= 0;
		xix[0] = 0.0;
		xiy[0] = 0.0;
		xiz[0] = 0.0;
		_xix[0] = 0.0;
		_xiy[0] = 0.0;
		_xiz[0] = 0.0;
        
		FGrav.reset();
		FHydro.reset();
        FContact.reset();
        FLub.reset();
        FTotal.reset();
		FRep.reset();
		
		SHydro.reset();
		SLub.reset();
		SContact.reset();
		SRep.reset();
		STotal.reset();
		
		

		MHydro.reset();
        MContact.reset();
        MLub.reset();
        MTotal.reset();

        fluidVolume=0.0;
    }
    void elmtShow()const;
    void initialize(const double& partDensity, tVect& demF, unsigned int& globalIndex);
    void resetVelocity();
};

class object{
public:
    // object index
    unsigned int index;
    // original object index in case of copies
    // element index
    // particle radius
    double r;
	double m;
	double _m;

    // position of the object
    tVect x0;
	tVect _x0;
    // velocity of the object
    tVect x1;
    // force on the object
    tVect FContact,FHydro, FLub, FGrav, FRep, FTotal;
    object() {
        index=0;
        r=0.0;
		_m=m=1.0;
        _x0=x0=tVect(0.0,0.0,0.0);
        x1=tVect(0.0,0.0,0.0);
        FContact=tVect(0.0,0.0,0.0);
        FHydro=tVect(0.0,0.0,0.0);
		FGrav=tVect(0.0,0.0,0.0);
		FRep=tVect(0.0,0.0,0.0);
		FLub=tVect(0.0,0.0,0.0);
		FTotal=tVect(0.0,0.0,0.0);
		
    }
};


class material{
public:
    // density [ mass / length² ]
    double density;
	
	double objectDensity;

    // linear model ////////////////////
    // stiffness
    double linearStiff;

    // normal damping ///////////////////////////
    // normal viscous coefficient
    double dampCoeff;

    // tangential model ////////////////////////
    // particle particle friction
    double frictionCoefPart;
    // particle-wall friction
	
	double lubNormal;
	double lubTangential;
	double lubRotation;
	
	double lubCutoff;
	double kinVisc;
	
	double electroForce;
	double debyeLength;


    // default constructor
    material(){
        density=1.0;
        linearStiff=0.0;
        dampCoeff=0.0;
        frictionCoefPart=0.0;
		lubNormal = 0.0;
		lubTangential = 0.0;
		lubRotation = 0.0;
		lubCutoff = 0.0;
		kinVisc = 0.0;
    }
};



#endif	/* ELMT_H */

